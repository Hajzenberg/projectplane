package app;


import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.*;

import javax.imageio.ImageIO;

public class Util {

	public static Random random = new Random();

	public static GregorianCalendar calendar = new GregorianCalendar();

	public static BufferedImage ucitajSliku(String path) {
		BufferedImage image = null;
		try {
			image = ImageIO.read(Util.class.getResource(path));
			// image = ImageIO.read(new File(path));
		} catch (IOException e) {
			System.out.println("Slika nije ucitana, ouch!\n");
			e.printStackTrace();
		}
		return image;
	}
}
