package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class Raketa implements Municija {

	private double x,y;
	private BufferedImage image;
	
	@Override
	public void tick() {
		y -= 10;
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(image, (int) x, (int) y, null);	
	}

	@Override
	public double getY() {
		return y;
	}

	@Override
	public Rectangle2D getBounds() {
		return new Rectangle2D.Double(x,y,20,75);
	}

	public Raketa(double x, double y, boolean tip) {
		super();
		this.x = x;
		this.y = y;
		image = Util.ucitajSliku("/res/raketa20.png");
	}
	
	public String toString (){
		return x+";"+y+";";
	}

}
