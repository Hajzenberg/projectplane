package app;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class Input implements KeyListener, MouseListener {

	Igra igra;
	private int brojIspaljenihRaketa=1;
	private boolean desno;			//Promenljiva koja odredjuje sa koje strane se lansira raketa
	private boolean puca;			//Proverava da li igrac vec ispaljue metak, ukoliko ispaljuje ne moze ispaliti sledeci
									//sve dok ne otpusti SPACE i pritisne ga ponovo, logika je realizovana u keyPressed() i KeyReleased() metodama
	public Input(Igra igra) {
		this.igra = igra;
	}

	@Override
	public void keyPressed(KeyEvent e) {
		int taster = e.getKeyCode();

		switch (taster) {
		case KeyEvent.VK_UP:
			// igra.getIgrac().setY(igra.getIgrac().getY()-5);
			igra.getIgrac().setBrzinaY(-5);
			break;
		case KeyEvent.VK_DOWN:
			// igra.getIgrac().setY(igra.getIgrac().getY()+5);
			igra.getIgrac().setBrzinaY(5);
			break;
		case KeyEvent.VK_LEFT:
			// igra.getIgrac().setX(igra.getIgrac().getX()-5);
			igra.getIgrac().setBrzinaX(-5);
			break;
		case KeyEvent.VK_RIGHT:
			// igra.getIgrac().setX(igra.getIgrac().getX()+5);
			igra.getIgrac().setBrzinaX(5);
			break;

		case KeyEvent.VK_SPACE:
			// igra.getIgrac().setX(igra.getIgrac().getX()+5);
			if (!puca){
				
				if (Igra.brojSrusenih<=10){
					igra.getMunicija().addLater(new Metak(igra.getIgrac().getX()+46, igra.getIgrac().getY(),true));
				} else {
					igra.getMunicija().addLater(new Metak(igra.getIgrac().getX()+18, igra.getIgrac().getY()+70,true));
					igra.getMunicija().addLater(new Metak(igra.getIgrac().getX()+75, igra.getIgrac().getY()+70,true));
				}
				
				puca=true;
			}
			break;
			
		case KeyEvent.VK_CONTROL:
			// igra.getIgrac().setX(igra.getIgrac().getX()+5);
			if (igra.getIgrac().getBrojRaketa()>0 && !puca){
				puca = true;
				
				if (desno){
					igra.getMunicija().addLater(new Raketa(igra.getIgrac().getX()+63, igra.getIgrac().getY()+46, true));
					desno=false;
				} else {
					igra.getMunicija().addLater(new Raketa(igra.getIgrac().getX()+18, igra.getIgrac().getY()+46, true));
					desno=true;
				}
				
				igra.getIgrac().setBrojRaketa(-brojIspaljenihRaketa);
			}
			break;
			
		case KeyEvent.VK_ESCAPE:
			if (igra.getStanjeIgre()!=StanjeIgre.GLAVNI_MENI){
				igra.setPozadina(Util.ucitajSliku("/res/skylong.jpg"));
				igra.setStanjeIgre(StanjeIgre.GLAVNI_MENI);
			}
			break;
			
		case KeyEvent.VK_S:
			if (igra.getStanjeIgre()==StanjeIgre.IGRA){
				igra.setPauziran(true);
				igra.sacuvajIgru();
				igra.setPauziran(false);
			}
			break;
			
		case KeyEvent.VK_P:
			if (igra.getStanjeIgre()==StanjeIgre.IGRA){
				if (igra.isPauziran()){
					igra.setPauziran(false);
				} else {
					igra.setPauziran(true);
				}
			}
			break;

		default:
			break;
		}
		

	}

	@Override
	public void keyReleased(KeyEvent e) {
		int taster = e.getKeyCode();
		
		switch (taster) {
		case KeyEvent.VK_UP:
			// igra.getIgrac().setY(igra.getIgrac().getY()-5);
			igra.getIgrac().setBrzinaY(0);
			break;
		case KeyEvent.VK_DOWN:
			// igra.getIgrac().setY(igra.getIgrac().getY()+5);
			igra.getIgrac().setBrzinaY(0);
			break;
		case KeyEvent.VK_LEFT:
			// igra.getIgrac().setX(igra.getIgrac().getX()-5);
			igra.getIgrac().setBrzinaX(0);
			break;
		case KeyEvent.VK_RIGHT:
			// igra.getIgrac().setX(igra.getIgrac().getX()+5);
			igra.getIgrac().setBrzinaX(0);
			break;
		case KeyEvent.VK_SPACE:
			puca=false;
			break;
		case KeyEvent.VK_CONTROL:
			puca=false;
			break;

		default:
			break;
		}

	}

	@Override
	public void keyTyped(KeyEvent arg0) {
		// TODO Auto-generated method stub

	}

	@Override
	public void mouseClicked(MouseEvent e) {
		if (e.getClickCount()>2 && e.getButton()==1){
			igra.setNeunistivost(true);
			System.out.println("Postali ste neunistivi!");
		}
		
		if (e.getClickCount()>2 && e.getButton()==3){
			brojIspaljenihRaketa=0;
			System.out.println("999 raketa");
		}
	}

	@Override
	public void mouseEntered(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mouseExited(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void mousePressed(MouseEvent e) {
		if (e.getX()>=180 && e.getX()<=425 && e.getY()>=350 && e.getY()<=420 && igra.getStanjeIgre()==StanjeIgre.GLAVNI_MENI){
			igra.setPozicijaPozadine(-3700);
			igra.setStanjeIgre(StanjeIgre.IGRA);
		} else if (e.getX()>=180 && e.getX()<=425 && e.getY()>=430 && e.getY()<=500 && igra.getStanjeIgre()==StanjeIgre.GLAVNI_MENI){
			igra.ucitajIgru();
		} else if (e.getX()>=180 && e.getX()<=425 && e.getY()>=515 && e.getY()<=585 && igra.getStanjeIgre()==StanjeIgre.GLAVNI_MENI){
			igra.setStanjeIgre(StanjeIgre.O_IGRI);
		} else if (e.getX()>=180 && e.getX()<=425 && e.getY()>=600 && e.getY()<=670 && igra.getStanjeIgre()==StanjeIgre.GLAVNI_MENI){
			System.exit(0);
		}
	}

	@Override
	public void mouseReleased(MouseEvent arg0) {
		// TODO Auto-generated method stub
		
	}

}
