package app;

import java.awt.*;
import java.awt.image.BufferedImage;

public class HUD {
	
	private BufferedImage krug = Util.ucitajSliku("/res/krug80visetransparentan.png");
	private BufferedImage ostecenje = Util.ucitajSliku("/res/ostecenje170.png");
	private BufferedImage kvadrat = Util.ucitajSliku("/res/missilebar.png");
	private BufferedImage meni = Util.ucitajSliku("/res/meni550.png");
	private BufferedImage steta = Util.ucitajSliku("/res/steta.png");
	private BufferedImage porazTekst = Util.ucitajSliku("/res/poraztekst.png");
	private BufferedImage pobedaTekst = Util.ucitajSliku("/res/pobedatekst.png");
	private BufferedImage oIgriTekst = Util.ucitajSliku("/res/oigri.png");
	private String ispisSrusenih;
	private Font fontZaIspisSrusenih = new Font("Courier", Font.BOLD,42);
	
	void glavniMeni (Graphics2D g2d){
		g2d.drawImage(meni, 22, 100, null);
	}
	
	public void izgubio (Graphics2D g2d){
		g2d.drawImage(porazTekst, 30, 150, null);
	}
	
	public void pobedio (Graphics2D g2d){
		g2d.drawImage(pobedaTekst, 30, 150, null);
	}
	
	public void oIgri (Graphics2D g2d){
		g2d.drawImage(oIgriTekst, 25, 70, null);
	}
	
	public void podaci (Graphics2D g2d, int brojSrusenih, int HP, int brojRaketa){
		
		g2d.drawImage(krug, 20, 670, null);
		g2d.setFont(fontZaIspisSrusenih);
		
		if (brojSrusenih<10){
			ispisSrusenih="0"+brojSrusenih;
		} else {
			ispisSrusenih=""+brojSrusenih;
		}
		
		g2d.drawString(ispisSrusenih, 35, 722);
		g2d.drawImage(ostecenje, 400, 660, null);
		g2d.drawImage(steta, 402,680, 402+HP*16+HP/2, 700, 5, 5,10,10,null  );
		
		for (int i = 0; i < brojRaketa; i++) {
			g2d.drawImage(kvadrat, 402+i*24, 722, null);
		}
	}
}
