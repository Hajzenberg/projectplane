package app;

import javax.swing.JFrame;

public class Window extends JFrame{

	private static final long serialVersionUID = 1L;
	public static int WIDTH = 600;
	public static int HEIGHT = 800;
	
	public Window (){
		
		setTitle("SLOBODNO NEBO");
		setSize(WIDTH, HEIGHT);
		setDefaultCloseOperation(EXIT_ON_CLOSE);
		setVisible(true);
		setLocationRelativeTo(null);
	}	
}
