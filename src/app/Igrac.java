package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class Igrac extends Avion {
	
	private BufferedImage image;
	private double brzinaX, brzinaY;						//Kretanje igraca po mapi se realizuje stalnim inkrementovanjem brzine po X i Y osi
	private int brojRaketa;									//umesto direktnim setovanjem x i y koordinata, kako bi se obezbedila dodatna glatkoca animacije

	public Igrac(double x, double y) {						
		super(x,y,10);
		image = Util.ucitajSliku("/res/avio100-154.png");
		brojRaketa=7;
	}

	public Igrac(double x, double y, int HP, int brojRaketa) {
		super(x, y, HP);
		this.brojRaketa = brojRaketa;
		image = Util.ucitajSliku("/res/avio100-154.png");
	}



	@Override
	public void tick() {									//Ono sto Igrac odradi za vreme jednog traktnog intervala
		setX(getX()+brzinaX);
		setY(getY()+brzinaY);
		
		if (getX()>Window.WIDTH-image.getWidth()){			//Ovi uslovi osiguravaju da igrac uvek bude u okvirima prozora
			setX(Window.WIDTH-image.getWidth());			
		} else if (getX()<0){
			setX(0);
		} else if (getY()>Window.HEIGHT-image.getHeight()-23){	
			setY(Window.HEIGHT-image.getHeight()-23);		//23 piksela je offset prozora u dnu ekrana
		} else if (getY()<0){
			setY(0);
		}
	}
	
	public void popraviSe(){
		setHP(10);
	}
	
	@Override												
	public void setHP(int HP){								//Seter metoda za HP je morala biti nadjacana da bi se obezbedio
		if (getHP()+HP>10){									//nesmetan rad metodi popraviSe() koja se aktivira kako bi se 
			super.setHP(10-getHP());						//realizovala neunistivost
		} else {
			super.setHP(HP);
		}
	}

	@Override
	public void render(Graphics g) {
		g.drawImage(image, (int)getX(), (int)getY(), null);
	}

	@Override
	public Rectangle2D.Double getBounds() {
		return new Rectangle2D.Double(getX(), getY(), image.getWidth(), image.getHeight());
	}

	public void setBrzinaX(double brzinaX) {
		this.brzinaX = brzinaX;
	}

	public void setBrzinaY(double brzinaY) {
		this.brzinaY = brzinaY;
	}

	public int getBrojRaketa() {
		return brojRaketa;
	}

	public void setBrojRaketa(int brojRaketa) {
		this.brojRaketa += brojRaketa;
	}
	
	public String toString(){
		return getX()+";"+getY()+";"+getHP()+";"+brojRaketa+";";
	}
}
