package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class Metak implements Municija {
	
	private double x,y;
	private BufferedImage image;
	private boolean igrac;			//Promenljiva odredjuje da li je metak ispaljen od strane igraca ili od strane neprijatelja

	public Metak(double x, double y, boolean tip) {
		super();
		this.x = x;
		this.y = y;
		image = Util.ucitajSliku("/res/metak12.png");
		this.igrac=tip;
	}

	public void tick() {
		if (igrac==true){
			y -= 10;
		} else {
			y += 10;
		}
	}

	public double getX() {
		return x;
	}

	public double getY() {
		return y;
	}

	public void render(Graphics g) {
		g.drawImage(image, (int) x, (int) y, null);
	}

	@Override
	public Rectangle2D getBounds() {
		return new Rectangle2D.Double(x,y,12,11);
	}

	public boolean isIgrac() {
		return igrac;
	}
	
	public String toString (){
		return x+";"+y+";"+igrac+";";
	}

}
