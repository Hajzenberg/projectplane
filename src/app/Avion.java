package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public abstract class Avion {

	private double x;
	private double y;
	private BufferedImage image;
	private int HP;
	
	public Avion(double x, double y, int HP) {
		super();
		this.x = x;
		this.y = y;
		this.HP=HP;
	}
	
	public abstract void tick();
	public abstract void render(Graphics g);
	
	public Rectangle2D.Double getBounds() {
		return new Rectangle2D.Double(this.x, this.y, image.getWidth(), image.getHeight());
	}
	
	public double getX() {
		return x;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public BufferedImage getImage() {
		return image;
	}

	public int getHP() {
		return HP;
	}

	public void setHP(int HP) {
		this.HP+=HP;
	}
}
