package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;
import java.awt.image.BufferedImage;

public class Neprijatelj extends Avion {

	private BufferedImage image;
	private int speed;
	private boolean state, levo, desno, naGore, naDole;
	private boolean ispalioMetke;
	private boolean kraljica;
	private Igra igra;
	private int redniBrojSlike;
	
	public void render(Graphics g) {
		g.drawImage(image, (int) getX(), (int) getY(), null);
	}
	
	public void tick(){

		//Kretanje po X osi, levo - desno
		
		if (levo){
			setX(getX()-speed/2);
			if (getX()<0){
				levo=false;
				desno=true;
			}
		
		} else if (desno){
			setX(getX()+speed/2);
			if (getX()>500){
				levo=true;
				desno=false;
			}
		}
		
		//Kretanje po Y osi, samo kraljica se krece gore-dole
		//obicni neprijatelji se krecu samo na dole
		
		if (!kraljica){
			setY(getY()+speed);
		} else if (kraljica) {
			if (naDole){
				setY(getY()+speed/2);
				if (getY()>=250){
					naDole=false;
					naGore=true;
				}
			} else if (naGore) {
				setY(getY()-speed/2);
				if (getY()<=-10){
					naDole=true;
					naGore=false;
				}
			}
		}
		
		//Ovaj uslov kontrolise pucanje obicnih neprijatelja
		//Zbog tezine igre, podeseno je da mogu samo jednom da ispale metak
		
		if (!ispalioMetke && !kraljica && getY()>100 && getY()<300){
			igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+image.getWidth()/2, getY()+image.getHeight()/2, false));
			ispalioMetke=true;
		}
		
		//Ovaj uslov kontrolise pucanje kraljice
		//Zbog tezine igre, podeseno da ispaljuje samo tri, umesto pet metaka odjednom
		
		if (kraljica && getY()%100==0){
			igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+50, getY()+80, false));
			igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+70, getY()+80, false));
			igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+30, getY()+80, false));
			//igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+80, getY()+80, false));
			//igra.getNeprijateljskaMunicija().addLater(new Metak(getX()+20, getY()+80, false));
		}
	}

	public Rectangle2D.Double getBounds() {														//Ova metoda vraca kvadrat koji ima iste koordinate kao slika neprijatelja
		return new Rectangle2D.Double(getX(), getY(), image.getWidth(), image.getHeight());		//Na osnovu tog kvadrata proveravam da li je doslo do kolizije sa drugim objektima
	}

	public Neprijatelj(int x, int redniBrojSlike, int HP, int speed, boolean kraljica, Igra igra) {
		super(x,-Igra.listaSlika.get(redniBrojSlike).getHeight()-Util.random.nextInt(Util.random.nextInt(150)+1), HP);	//Postavlja neprijatelja na zadatu x koordinatu
		this.image=Igra.listaSlika.get(redniBrojSlike);																	//y koordinata je fiksna i nalazi se za duzinu aviona + slucajan
		this.speed=speed;																								//broj iznad gornje ivice prozora (negativna je)
		state=Util.random.nextBoolean();
		levo=state;
		desno=!state;
		this.igra=igra;
		this.kraljica=kraljica;
		naDole=true;
		naGore=false;
		this.redniBrojSlike=redniBrojSlike;
	}
	
	public Neprijatelj (double x, double y, int speed, int HP, boolean state, boolean kraljica, boolean naDole, BufferedImage image, Igra igra){			
		super(x,y,HP);																
		this.state=state;
		this.speed=speed;
		levo=state;
		desno=!state;								//Ovo je konstruktor koji se koristi za stvaranje neprijatelja kad se vrsi ucitavanje iz fajla
		this.kraljica=kraljica;
		this.naDole=naDole;
		this.naGore=!naDole;
		this.igra=igra;
		this.image=image;
	}
	
	public String toString (){
		return getX()+";"+getY()+";"+speed+";"+getHP()+";"+state+";"+kraljica+";"+naDole+";"+redniBrojSlike+";";
	}

	public boolean isKraljica() {
		return kraljica;
	}	
}
