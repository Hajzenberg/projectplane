package app;

import java.awt.*;
import java.awt.image.*;
import java.io.*;
import java.util.*;
import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;

public class Igra extends Canvas implements Runnable {

	private static final long serialVersionUID = 1L;
	private boolean running = false;
	private Thread thread;
	private StanjeIgre stanjeIgre;
	private boolean neunistivost;						//Kada se klikne tri puta brzo na naslov u glavnom meniju igrac postaje neunistiv (stalno se poziva metoda popraviSe()
	private boolean kraljicaSePojavila;					//Oznacava da se pojavila kraljica na kraju nivoa i sprecava da se kraljice dalje pojavljuju
	public static int brojSrusenih=0;					//Broj unistenih aviona
	private int pozicijaPozadine;						//Ovom promenljivom se kontrolise kretanje pozadine
	private int brzina = 1;								//Brzina kojom se pozadina krece
	private HUD hud;
	private Igrac igrac;
	private boolean pauziran;
	
	private BufferedImage pozadina;
	private LazyArrayList<Municija> municija;
	private LazyArrayList<Municija> neprijateljskaMunicija;
	public static ArrayList<BufferedImage> listaSlika;
	private LazyArrayList<Neprijatelj> neprijatelji;
	private JFileChooser fileChooser;
	
	public Igra() {
		
		igrac = new Igrac(250, 530);
		hud = new HUD();
		municija = new LazyArrayList<Municija>();
		neprijateljskaMunicija = new LazyArrayList<Municija>();
		listaSlika = new ArrayList<BufferedImage>();
		neprijatelji = new LazyArrayList<Neprijatelj>();
		pozadina = Util.ucitajSliku("/res/skylong.jpg");
		fileChooser = new JFileChooser();
		listaSlika.add(Util.ucitajSliku("/res/enemyMIRAGE100.png"));
		listaSlika.add(Util.ucitajSliku("/res/enemyMIG.png"));
		listaSlika.add(Util.ucitajSliku("/res/enemyF22.png"));
		listaSlika.add(Util.ucitajSliku("/res/enemyA10120.png"));
		setStanjeIgre(StanjeIgre.GLAVNI_MENI);
	}

	public static void main(String[] args) {

		/* Da bi BufferStrategy radio, prvo mora Canvas da se doda u prozor,
		 * kako bi znao sta da baferuje
		 * igra.start() ne sme da se pozove pre nego sto je igra dodata u prozor,
		 * kako thread ne bi mogao da pozove render() pre nego sto je sama igra dodata
		*/
		
		Window prozor = new Window();
		Igra igra = new Igra();
		Input input = new Input(igra);
		igra.addKeyListener(input);
		igra.addMouseListener(input);
		prozor.add(igra);
		igra.requestFocus();
		igra.start();
		
		//Zbog problema sa renederovanjem koje klasa CANVAS ima na nekim sistemima
		//vrsim promenu velicine prozora pri pokretanju programa kako bi se
		//osigurao refrsh i pojavila slika
		
		prozor.setSize(600, 800);
		prozor.setResizable(false);
	}

	private void start() {
		if (running) {
			return;
		} else {
			running = true;
			thread = new Thread(this);
			thread.setDaemon(true);
			thread.start();
		}
	}

	private void stop() {
		if (!running) {
			return;
		} else {
			running = false;
			try {
				thread.join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			System.exit(1);
		}
	}

	@Override
	public void run() {
		long prethodnoVreme = System.nanoTime();			
		final double brojTickova = 60.0;						//Podeseno je da se igra osvezava na svakih 16 milisekundi, odnosno 60 puta (tick-ova) u sekundi
		double trajanjeTicka = 1000000000 / brojTickova;		//Taj broj predstavlja najbolji odnos izmedju udara na performanse i glatkoce animacije na ekranu
		double delta = 0;

		while (running) {

			//if (!pauziran){
					long trenutnoVreme = System.nanoTime();
					delta += (trenutnoVreme - prethodnoVreme) / trajanjeTicka;
					prethodnoVreme = trenutnoVreme;
					if (delta >= 1) {
						if (!pauziran){
							tick();
						}

						try {
							Thread.sleep(16);
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
						delta--;
					}
					
					if (!pauziran){
						render();
					}
			//}
		}
		stop();
	}

	private synchronized void tick() {
		
		if (stanjeIgre==StanjeIgre.GLAVNI_MENI){
			if (pozicijaPozadine<=-5){
				pozicijaPozadine+=brzina;
			}
		}
		
		if (stanjeIgre==StanjeIgre.O_IGRI){
			if (pozicijaPozadine<=-5){
				pozicijaPozadine+=brzina;
			}
		}
		
		if (stanjeIgre==StanjeIgre.IZGUBIO){
			if (pozicijaPozadine<=-5){
				pozicijaPozadine+=brzina;
			}
		}
		
		if (stanjeIgre==StanjeIgre.POBEDIO){
			if (pozicijaPozadine<=-5){
				pozicijaPozadine+=brzina;
			}
		}
		
		if (stanjeIgre==StanjeIgre.IGRA){
			
			if (neunistivost && igrac.getHP()<10){											//Ukoliko je podesena neunistivost, ukoliko igrac bude ostecen
				igrac.popraviSe();															//poziva se metoda popraviSe(), koja ga potpuno popravlja
			}
			
			if (pozicijaPozadine<=-5){														//Ovde se pomera pozadina sve dok se ne dodje do kraja nivoa
				pozicijaPozadine+=brzina;
			}
			
			igrac.tick();																	//
			
			for (Municija tempMunicija : municija) {
				tempMunicija.tick();
				if (tempMunicija.getY()<=0){
					municija.removeLater(tempMunicija);
				}
			}
			
			for (Neprijatelj tempNeprijatelj : neprijatelji) {
				tempNeprijatelj.tick();
				for (Municija tempMunicija : municija) {
					
					if (tempNeprijatelj.getBounds().intersects(tempMunicija.getBounds())){
						
						if (tempMunicija instanceof Metak){
							tempNeprijatelj.setHP(-1);
						} else {
							tempNeprijatelj.setHP(-3);
						}
						
						if (tempNeprijatelj.getHP()<=0){
							neprijatelji.removeLater(tempNeprijatelj);
							brojSrusenih++;
							if (tempNeprijatelj.isKraljica()){
								setStanjeIgre(StanjeIgre.POBEDIO);
							}
						}
						municija.removeLater(tempMunicija);
					}
				}
				if (tempNeprijatelj.getY()>=900){
					neprijatelji.removeLater(tempNeprijatelj);
				}
			}
			
			for (Municija tempNeprijateljskaMunicija : neprijateljskaMunicija) {
				tempNeprijateljskaMunicija.tick();

				if (tempNeprijateljskaMunicija.getBounds().intersects(igrac.getBounds())){
					neprijateljskaMunicija.removeLater(tempNeprijateljskaMunicija);
					igrac.setHP(-1);
				} else if (tempNeprijateljskaMunicija.getY()>=800){
					neprijateljskaMunicija.removeLater(tempNeprijateljskaMunicija);
				}
			}
			neprijateljskaMunicija.restructure();
			
			//U predefinisanim intervalima se na mapu dodaju neprijatelji
			//Na kraju mape se pojavljuje poslednji neprijatelj, takozvana "kraljica"
			
			if (pozicijaPozadine%300==0){		
				neprijatelji.addLater(new Neprijatelj(0+Util.random.nextInt(150),Util.random.nextInt(4),3,3,false,this));
				neprijatelji.addLater(new Neprijatelj(250+Util.random.nextInt(150),Util.random.nextInt(4),3,2,false,this));
				neprijatelji.addLater(new Neprijatelj(500+Util.random.nextInt(150),Util.random.nextInt(4),3,5, false,this));
			} else if (pozicijaPozadine>=-5 && !kraljicaSePojavila){
				neprijatelji.addLater(new Neprijatelj(280,2,20,3,true,this));
				kraljicaSePojavila=true;
			}
			
			neprijatelji.restructure();
			municija.restructure();
			
			if (igrac.getHP()<=0){
				setStanjeIgre(StanjeIgre.IZGUBIO);
			}
		}
	}

	private synchronized void render() {
		
		BufferStrategy bafer = getBufferStrategy(); // Prvo metodom getBufferStrategy, koja je nasledjena iz Canvas-a inicijalizujemo bafere
		if (bafer == null) {
			createBufferStrategy(3); // kreira triple buffer -> slika, drugi bafer, treci bafer
			return;
		}

		Graphics g = bafer.getDrawGraphics(); // vadi parametre za crtanje grafike iz bafera
		Graphics2D g2d = (Graphics2D) g;
		
		/* SVE STO SE ISCRTAVA SE NALAZI ISPOD */
		
		g2d.drawImage(pozadina, 0, pozicijaPozadine, this);

		if (stanjeIgre==StanjeIgre.GLAVNI_MENI){
			
			hud.glavniMeni(g2d);
		}

		if (stanjeIgre==StanjeIgre.IGRA){
			
			for (Municija tempMunicija : municija) {
				tempMunicija.render(g2d);
			}
			
			for (Municija tempNeprijateljskamunicija : neprijateljskaMunicija) {
				tempNeprijateljskamunicija.render(g2d);
			}
			
			for (Neprijatelj neprijatelj : neprijatelji) {
				neprijatelj.render(g2d);
			}
			
			igrac.render(g);
			
			hud.podaci(g2d, brojSrusenih, igrac.getHP(), igrac.getBrojRaketa());
		}
		
		if (stanjeIgre==StanjeIgre.O_IGRI){
			hud.oIgri(g2d);
		}
		
		if (stanjeIgre==StanjeIgre.IZGUBIO){
			hud.izgubio(g2d);
		}
		
		if (stanjeIgre==StanjeIgre.POBEDIO){
			hud.pobedio(g2d);
		}
		
		g.dispose();		//Posto je iscrtavanja zavrseno, "cistimo" g
		bafer.show();		//Prazni se prvi bafer i prikazuje se slika koja se nalazila u njemu. 
	}
	
	//Metoda kojom se podesava u kom je stanju igra
	//Podesavanje se vrsi u klasi Input, na osnovu komandi koje je uneo korisnik
	
	public void setStanjeIgre(StanjeIgre stanjeIgre) {
		this.stanjeIgre = stanjeIgre;
		if (stanjeIgre==StanjeIgre.GLAVNI_MENI){
			resetIgre();
			pozicijaPozadine=-6300;
		} else if (stanjeIgre==StanjeIgre.IGRA){
			//pozicijuPozadine setujem u Input klasi, da ne bi doslo do konflikta prilikom cuvanja igre
		} else if (stanjeIgre==StanjeIgre.O_IGRI){
			pozicijaPozadine=-6300;
		} else if (stanjeIgre==StanjeIgre.IZGUBIO){
			pozadina=Util.ucitajSliku("/res/porazpozadina.jpg");
			pozicijaPozadine=-700;
		} else if (stanjeIgre==StanjeIgre.POBEDIO){
			pozadina=Util.ucitajSliku("/res/pobedapozadina.jpg");
			pozicijaPozadine=-530;
		}
	}
	
	//Prilikom izlaska iz igre u glavni meni, svi parametri igre se resetuju na pocetne
	//kako bi bili spremni za ponovno pokretanje
	
	public void resetIgre(){
		municija.clear();
		neprijateljskaMunicija.clear();
		neprijatelji.clear();
		igrac = new Igrac(250, 530);
		brojSrusenih=0;
		kraljicaSePojavila=false;
	}
	
	public void sacuvajIgru(){
		
		PrintWriter writer=null;
		
		fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
		
		int returnValue = fileChooser.showOpenDialog(null);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
	         
			File fajl = fileChooser.getSelectedFile();
	        String imeFajla = fajl.getAbsolutePath()+'\\'+"SACUVANO.avioni";
	        
	  		try {
	  			writer = new PrintWriter(imeFajla);
	  		} catch (FileNotFoundException e) {
	  			//e.printStackTrace();
	  			JOptionPane.showMessageDialog(null, "NAPRAVILI STE GRESKU PRI ODABIRU FOLDERA ZA CUVANJE IGRE", "GRESKA PRI ODABIRU FOLDERA", JOptionPane.ERROR_MESSAGE);
	  			return;
	  		}
	  		
	  		writer.println("kraljicaSePojavila;"+kraljicaSePojavila+";");
	  		writer.println("pozicijaPozadine;"+pozicijaPozadine+";");
	  		writer.println("brojSrusenih;"+brojSrusenih+";");
	  		writer.println("igrac;"+igrac.toString());
	  		
	  		for (Neprijatelj neprijatelj : neprijatelji) {
	  			writer.println("neprijatelj;"+neprijatelj.toString());
	  		}
	  		
	  		for (Municija municija: neprijateljskaMunicija){
	  			writer.println("neprijateljskaMunicija;"+municija.toString());
	  		}
	  		
	  		for (Municija municija: municija){
	  			writer.println("municija;"+municija.toString());
	  		}
	  		
	  		writer.close();
		}
	}
	
	public void ucitajIgru (){
		
		BufferedReader citac=null;
		File fajl=null;
		String temp="";
		
		fileChooser.setFileSelectionMode(JFileChooser.FILES_ONLY);
		fileChooser.setFileFilter(new FileNameExtensionFilter("SACUVANI FAJL", "avioni"));
		int returnValue = fileChooser.showOpenDialog(null);
		
		if (returnValue == JFileChooser.APPROVE_OPTION) {
	          fajl = fileChooser.getSelectedFile();
	          
	          try {
	  			citac = new BufferedReader(new FileReader(fajl));
	  		} catch (FileNotFoundException e) {
	  			//e.printStackTrace();
	  			JOptionPane.showMessageDialog(null, "NAPRAVILI STE GRESKU PRI ODABIRU FAJLA", "GRESKA PRI ODABIRU FAJLA", JOptionPane.ERROR_MESSAGE);
	  			return;
	  		}
	  		
	  		try {
	  			while((temp = citac.readLine())!=null) {
	  				StringTokenizer st = new StringTokenizer(temp, ";");
	  				
	  				String prvaRec = st.nextToken();
	  				
	  				if (prvaRec.equals("kraljicaSePojavila")){
	  					kraljicaSePojavila=Boolean.parseBoolean(st.nextToken());
	  				}
	  				else if (prvaRec.equals("pozicijaPozadine")){
	  					pozicijaPozadine=Integer.parseInt(st.nextToken());
	  				}
	  				else if (prvaRec.equals("brojSrusenih")){
	  					brojSrusenih=Integer.parseInt(st.nextToken());
	  				} else if (prvaRec.equals("igrac")){
	  					igrac=new Igrac(Double.parseDouble(st.nextToken()), Double.parseDouble(st.nextToken()),Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()));
	  				} else if (prvaRec.equals("neprijatelj")){
	  					neprijatelji.add(new Neprijatelj(Double.parseDouble(st.nextToken()),Double.parseDouble(st.nextToken()),Integer.parseInt(st.nextToken()),Integer.parseInt(st.nextToken()),Boolean.parseBoolean(st.nextToken()),Boolean.parseBoolean(st.nextToken()),Boolean.parseBoolean(st.nextToken()),listaSlika.get(Integer.parseInt(st.nextToken())),this));
	  				}
	  			}
	  		} catch (NumberFormatException | IOException e1) {
	  			e1.printStackTrace();
	  		}

	  		try {
	  			citac.close();
	  		} catch (IOException e) {
	  			e.printStackTrace();
	  		}
	  		
	  		stanjeIgre=StanjeIgre.IGRA;
		} else {
			//System.out.println("ODUSTAO SI OD UCITAVANJA FAJLA, OK...");
		}
	}

	public LazyArrayList<Municija> getNeprijateljskaMunicija() {
		return neprijateljskaMunicija;
	}
	
	public StanjeIgre getStanjeIgre() {
		return stanjeIgre;
	}

	public boolean isNeunistivost() {
		return neunistivost;
	}

	public void setNeunistivost(boolean neunistivost) {
		this.neunistivost = neunistivost;
	}

	public void setPozadina(BufferedImage pozadina) {
		this.pozadina = pozadina;
	}

	public void setPozicijaPozadine(int pozicijaPozadine) {
		this.pozicijaPozadine = pozicijaPozadine;
	}
	
	public Igrac getIgrac() {
		return igrac;
	}

	public LazyArrayList<Municija> getMunicija() {
		return municija;
	}

	public void setSpeed(int speed) {
		this.brzina = speed;
	}

	public void setPauziran(boolean pauziran) {
		this.pauziran = pauziran;
	}

	public boolean isPauziran() {
		return pauziran;
	}
	
}
	