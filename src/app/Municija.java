package app;

import java.awt.Graphics;
import java.awt.geom.Rectangle2D;

public interface Municija {
	
	void tick();

	void render(Graphics g);
	
	public Rectangle2D getBounds();
	
	double getY();

}
