package app;

import java.util.ArrayList;

public class LazyArrayList<T> extends ArrayList<T> {

	private static final long serialVersionUID = 1L;
	private ArrayList<T> append=new ArrayList<T>();
	private ArrayList<T> remove=new ArrayList<T>();
	
	public void addLater(T item) {
		append.add(item);
	}
	public void removeLater(T item) {
		remove.add(item);
	}
	public void restructure() {
		removeAll(remove);
		remove.clear();
		addAll(append);
		append.clear();
	}
}